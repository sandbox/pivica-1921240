; Drupal Mailing system make file
; Check http://drupal.org/node/1432374 for example of make file.

; The Drush Make API version. This should always be 2.
api = 2

; The version of Drupal the profile is built for.
core = 7.20

; Contrib dependencies.
projects[admin_menu] = 3.0-rc4
projects[ctools] = 1.2
projects[entity] = 1.0
projects[views] = 3.5
projects[views_bulk_operations] = 3.1
projects[token] = 1.4

; Optional cotrib dependencies.
projects[ultimate_cron] = 1.8
projects[progress] = 1.0
projects[background_process] = 1.3
projects[swiftmailer] = 1.3
; Remove this patch when https://drupal.org/node/1902628#comment-7091108 is fixed.
projects[swiftmailer][patch][] = "https://drupal.org/files/swiftmailer_attach.patch"
projects[mailsystem] = 2.34
projects[libraries] = 2.0
; RedHen is needed for CRM managment part.
projects[redhen] = 1.0
; Feeds and it dependency job_scheduler is needed by mailing import/export.
projects[feeds] = 2.0-alpha7
projects[job_scheduler] = 2.0-alpha3

; Contrib dependencies that need checking do we need them or they are temporary.
projects[media] = 1.2
projects[rules] = 2.2

; Temporary contrib dependencies - will be removed in the future.
projects[date] = 2.6

; Development contrib dependencies - only usefull for development.
projects[smtp] = 1.0

; Mailing module.
projects[mailing][type] = module
projects[mailing][download][type] = git
projects[draggableviews][download][branch] = master

; Libraries we need.
libraries[aloha][download][type] = get
libraries[aloha][download][url] = http://aloha-editor.org/builds/stable/alohaeditor-0.22.7.zip
libraries[aloha][destination] = libraries
libraries[emogrifier][download][type] = get
libraries[emogrifier][download][url] = http://www.pelagodesign.com/emogrifier/emogrifier.zip
libraries[emogrifier][destination] = libraries
libraries[swiftmailer][download][type] = get
libraries[swiftmailer][download][url] = http://swiftmailer.org/download_file/Swift-4.3.0.tar.gz
libraries[swiftmailer][destination] = libraries
